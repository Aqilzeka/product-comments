package dev.patika.productcomments.mapper;

import dev.patika.productcomments.dto.request.CommentRequest;
import dev.patika.productcomments.dto.response.CommentResponse;
import dev.patika.productcomments.entity.Comment;
import java.util.List;
import org.mapstruct.AfterMapping;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface CommentMapper {

    @Mapping(target = "product.id", source = "commentRequest.productId")
    @Mapping(target = "user.id", source = "commentRequest.userId")
    Comment requestToEntity(CommentRequest commentRequest);

    List<CommentResponse> entityListToResponseList(List<Comment> sourceComment);

    @AfterMapping
    default void setIds(List<Comment> sourceComments, @MappingTarget List<CommentResponse> targetComments) {
        for (int i = 0; i < sourceComments.size(); i++) {
            targetComments.get(i).setProductId(sourceComments.get(i).getProduct().getId());
            targetComments.get(i).setUserId(sourceComments.get(i).getUser().getId());
        }
    }
}
