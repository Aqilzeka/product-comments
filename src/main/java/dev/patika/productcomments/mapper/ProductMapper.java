package dev.patika.productcomments.mapper;

import dev.patika.productcomments.dto.request.ProductRequest;
import dev.patika.productcomments.dto.response.ProductResponse;
import dev.patika.productcomments.entity.Product;
import java.util.List;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface ProductMapper {
    Product requestToEntity(ProductRequest productRequest);

    List<ProductResponse> entityListToResponseList(List<Product> products);

}
