package dev.patika.productcomments.mapper;

import dev.patika.productcomments.dto.request.UserRequest;
import dev.patika.productcomments.entity.User;
import java.util.List;
import org.mapstruct.Builder;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring",
        builder = @Builder(disableBuilder = true),
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UserMapper {
    User requestToEntity(UserRequest userRequest);

    UserRequest entityToRequest(User user);

    List<UserRequest> entityListToRequestList(Iterable<User> users);
}
