package dev.patika.productcomments.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CommentRequest {
    @Length(max = 500, message = "Comment must not be longer than 500 characters!")
    @NotEmpty(message = "Comment must not be empty")
    private String comment;

    @Min(value = 1, message = "Product id must be greater than 0")
    private Long productId;

    @Min(value = 1, message = "User id must be greater than 0")
    private Long userId;
}
