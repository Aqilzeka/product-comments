package dev.patika.productcomments.dto.request;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

    @Length(max = 50, message = "Name length must not be longer than 50 characters!")
    @NotEmpty(message = "Name must not be empty")
    private String name;

    @Length(max = 50, message = "Lastname length must not be longer than 50 characters!")
    private String lastname;

    @Length(max = 50, message = "Email length must not be longer than 50 characters!")
    @Email(message = "Email pattern not valid")
    private String email;

    @Length(max = 15, message = "Number length must not be longer than 15 characters!")
    private String number;
}
