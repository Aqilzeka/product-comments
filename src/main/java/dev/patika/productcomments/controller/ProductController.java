package dev.patika.productcomments.controller;

import dev.patika.productcomments.dto.request.ProductRequest;
import dev.patika.productcomments.dto.response.ProductResponse;
import dev.patika.productcomments.service.ProductService;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Valid
@RestController
@RequestMapping("/products")
@AllArgsConstructor
public class ProductController {

    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void createProduct(@RequestBody ProductRequest productRequest) {
        productService.createProduct(productRequest);
    }

    @GetMapping("/expired")
    ResponseEntity<List<ProductResponse>> getProductsByExpirationDateBefore(
            @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime expirationDate) {
        return ResponseEntity.ok(productService.getProductsByExpirationDateBefore(expirationDate));
    }

    @GetMapping("/non-expired")
    ResponseEntity<List<ProductResponse>> getProductsByExpirationDateAfter(
            @RequestParam("date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime expirationDate) {
        return ResponseEntity.ok(productService.getProductsByExpirationDateAfter(expirationDate));
    }
}
