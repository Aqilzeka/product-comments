package dev.patika.productcomments.controller;

import dev.patika.productcomments.dto.request.CommentRequest;
import dev.patika.productcomments.dto.response.CommentResponse;
import dev.patika.productcomments.service.CommentService;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Valid
@RestController
@RequestMapping("/comments")
@AllArgsConstructor
public class CommentController {

    private final CommentService commentService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    void createComment(@RequestBody CommentRequest comment) {
        commentService.createComment(comment);
    }

    @GetMapping("product/{product-id}")
    ResponseEntity<List<CommentResponse>> getAllCommentByProductId(@PathVariable("product-id") Long productId) {
        return ResponseEntity.ok(commentService.getAllCommentByProductId(productId));
    }

    @GetMapping("product/{product-id}/date")
    ResponseEntity<List<CommentResponse>> getAllCommentByProductIdAndCommentedDateBetween(
            @PathVariable("product-id") Long productId,
            @RequestParam("first-date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime firstDate,
            @RequestParam("second-date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime secondDate) {

        return ResponseEntity.ok(
                commentService.getAllCommentByProductIdAndCommentedDateBetween(productId, firstDate, secondDate));
    }

    @GetMapping("user/{user-id}")
    ResponseEntity<List<CommentResponse>> getAllCommentByUserId(@PathVariable("user-id") Long userId) {
        return ResponseEntity.ok(commentService.getAllCommentByUserId(userId));
    }

    @GetMapping("user/{user-id}/date")
    ResponseEntity<List<CommentResponse>> getAllCommentByUserIdAndCommentDateBetween(
            @PathVariable("user-id") Long userId,
            @RequestParam("first-date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime firstDate,
            @RequestParam("second-date") @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm") LocalDateTime secondDate) {

        return ResponseEntity.ok(
                commentService.getAllCommentByUserIdAndCommentDateBetween(userId, firstDate, secondDate));
    }

}
