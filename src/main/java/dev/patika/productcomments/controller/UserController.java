package dev.patika.productcomments.controller;

import dev.patika.productcomments.dto.request.UserRequest;
import dev.patika.productcomments.service.UserService;
import java.util.List;
import javax.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Valid
@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    void createUser(@RequestBody @Valid UserRequest user) {
        userService.createUser(user);
    }

    @GetMapping("/id")
    ResponseEntity<UserRequest> getUserById(@PathVariable("id") Long userId) {
        return ResponseEntity.ok(userService.getUserById(userId));
    }

    @GetMapping
    ResponseEntity<List<UserRequest>> getAllUsers() {
        return ResponseEntity.ok(userService.getAllUsers());
    }
}
