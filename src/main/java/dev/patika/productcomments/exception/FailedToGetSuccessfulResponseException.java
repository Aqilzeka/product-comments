package dev.patika.productcomments.exception;

public class FailedToGetSuccessfulResponseException extends RuntimeException {
    public FailedToGetSuccessfulResponseException(String message) {
        super(message);
    }
}