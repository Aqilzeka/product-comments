package dev.patika.productcomments.repository;

import dev.patika.productcomments.entity.Product;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Query("select p from Product p where p.expirationDate <= :expirationDate")
    List<Product> findProductsByExpirationDateBefore(@Param("expirationDate") LocalDateTime expirationDate);


    List<Product> findAllByExpirationDateAfterOrExpirationDateIsNull(LocalDateTime expirationDate);
}
