package dev.patika.productcomments.repository;

import dev.patika.productcomments.entity.Comment;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Long> {
    List<Comment> findAllByProductId(Long productId);

    List<Comment> findAllByProductIdAndCommentedDateBetween(Long productId,
                                                            LocalDateTime firstDate,
                                                            LocalDateTime secondDate);

    List<Comment> findAllByUserId(Long userId);

    List<Comment> findAllByUserIdAndCommentedDateBetween(Long userId,
                                                         LocalDateTime firstDate,
                                                         LocalDateTime secondDate);

}
