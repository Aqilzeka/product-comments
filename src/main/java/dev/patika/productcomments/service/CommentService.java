package dev.patika.productcomments.service;

import dev.patika.productcomments.dto.request.CommentRequest;
import dev.patika.productcomments.dto.response.CommentResponse;
import java.time.LocalDateTime;
import java.util.List;

public interface CommentService {

    void createComment(CommentRequest comment);

    List<CommentResponse> getAllCommentByProductId(Long productId);

    List<CommentResponse> getAllCommentByProductIdAndCommentedDateBetween(Long productId,
                                                                          LocalDateTime firstDate,
                                                                          LocalDateTime secondDate);

    List<CommentResponse> getAllCommentByUserId(Long userId);

    List<CommentResponse> getAllCommentByUserIdAndCommentDateBetween(Long userId,
                                                                     LocalDateTime firstDate,
                                                                     LocalDateTime secondDate);
}
