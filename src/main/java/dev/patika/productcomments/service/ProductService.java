package dev.patika.productcomments.service;

import dev.patika.productcomments.dto.request.ProductRequest;
import dev.patika.productcomments.dto.response.ProductResponse;
import java.time.LocalDateTime;
import java.util.List;

public interface ProductService {

    void createProduct(ProductRequest productRequest);

    List<ProductResponse> getProductsByExpirationDateBefore(LocalDateTime expirationDate);

    List<ProductResponse> getProductsByExpirationDateAfter(LocalDateTime expirationDate);
}
