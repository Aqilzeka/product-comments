package dev.patika.productcomments.service.impl;

import dev.patika.productcomments.dto.request.CommentRequest;
import dev.patika.productcomments.dto.response.CommentResponse;
import dev.patika.productcomments.entity.Comment;
import dev.patika.productcomments.mapper.CommentMapper;
import dev.patika.productcomments.repository.CommentRepository;
import dev.patika.productcomments.service.CommentService;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    @Override
    public void createComment(CommentRequest commentRequest) {
        Comment comment = commentMapper.requestToEntity(commentRequest);
        log.info("CommentRequest mapped to comment {}", comment);
        commentRepository.save(comment);
    }

    @Override
    public List<CommentResponse> getAllCommentByProductId(Long productId) {
        List<Comment> comments = commentRepository.findAllByProductId(productId);
        log.info("Get all comments by productId {}", productId);
        return commentMapper.entityListToResponseList(comments);
    }

    @Override
    public List<CommentResponse> getAllCommentByProductIdAndCommentedDateBetween(Long productId,
                                                                                 LocalDateTime firstDate,
                                                                                 LocalDateTime secondDate) {
        List<Comment> comments =
                commentRepository.findAllByProductIdAndCommentedDateBetween(productId, firstDate, secondDate);
        log.info("Get all comments by productId: {} and between {} --- {} date", productId, firstDate, secondDate);
        return commentMapper.entityListToResponseList(comments);
    }

    @Override
    public List<CommentResponse> getAllCommentByUserId(Long userId) {
        List<Comment> comments = commentRepository.findAllByUserId(userId);
        log.info("Get all comments by userId {}", userId);
        return commentMapper.entityListToResponseList(comments);
    }

    @Override
    public List<CommentResponse> getAllCommentByUserIdAndCommentDateBetween(Long userId, LocalDateTime firstDate,
                                                                            LocalDateTime secondDate) {
        List<Comment> comments =
                commentRepository.findAllByUserIdAndCommentedDateBetween(userId, firstDate, secondDate);
        log.info("Get all comments by userId: {} and between {} --- {} date", userId, firstDate, secondDate);
        return commentMapper.entityListToResponseList(comments);
    }

}
