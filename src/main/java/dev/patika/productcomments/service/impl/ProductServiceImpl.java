package dev.patika.productcomments.service.impl;

import dev.patika.productcomments.dto.request.ProductRequest;
import dev.patika.productcomments.dto.response.ProductResponse;
import dev.patika.productcomments.entity.Product;
import dev.patika.productcomments.mapper.ProductMapper;
import dev.patika.productcomments.repository.ProductRepository;
import dev.patika.productcomments.service.ProductService;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;


    @Override
    public void createProduct(ProductRequest productRequest) {
        Product product = productMapper.requestToEntity(productRequest);
        log.info("ProductRequest mapped to product {}", product);
        productRepository.save(product);
    }

    @Override
    public List<ProductResponse> getProductsByExpirationDateBefore(LocalDateTime expirationDate) {
        List<Product> products = productRepository.findProductsByExpirationDateBefore(expirationDate);
        return productMapper.entityListToResponseList(products);
    }

    @Override
    public List<ProductResponse> getProductsByExpirationDateAfter(LocalDateTime expirationDate) {
        List<Product> products = productRepository.findAllByExpirationDateAfterOrExpirationDateIsNull(expirationDate);
        return productMapper.entityListToResponseList(products);
    }
}
