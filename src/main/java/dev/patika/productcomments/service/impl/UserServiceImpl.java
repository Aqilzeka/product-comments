package dev.patika.productcomments.service.impl;

import dev.patika.productcomments.dto.request.UserRequest;
import dev.patika.productcomments.entity.User;
import dev.patika.productcomments.exception.RecordNotFoundException;
import dev.patika.productcomments.mapper.UserMapper;
import dev.patika.productcomments.repository.UserRepository;
import dev.patika.productcomments.service.UserService;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public void createUser(UserRequest userRequest) {
        User user = userMapper.requestToEntity(userRequest);
        log.info("UserRequest mapped to user {}", user);
        userRepository.save(user);
    }

    @Override
    public UserRequest getUserById(Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new RecordNotFoundException(String.format("User not found by id: %s", userId)));
        UserRequest userRequest = userMapper.entityToRequest(user);
        log.info("User mapped to userRequest {}", user);
        return userRequest;
    }

    @Override
    public List<UserRequest> getAllUsers() {
        Iterable<User> users = userRepository.findAll();
        return userMapper.entityListToRequestList(users);
    }
}
