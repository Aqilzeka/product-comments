package dev.patika.productcomments.service;

import dev.patika.productcomments.dto.request.UserRequest;
import java.util.List;

public interface UserService {
    void createUser(UserRequest user);

    UserRequest getUserById(Long userId);

    List<UserRequest> getAllUsers();
}
